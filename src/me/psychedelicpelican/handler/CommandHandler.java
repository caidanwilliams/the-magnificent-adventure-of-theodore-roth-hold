package me.psychedelicpelican.handler;

import me.psychedelicpelican.story.Enemy;
import me.psychedelicpelican.map.Exit;
import me.psychedelicpelican.story.Item;
import me.psychedelicpelican.story.Room;
import me.psychedelicpelican.player.Player;

import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Created by Caidan Williams on 5/11/2016.
 */
public class CommandHandler {

    // Classes
    Player player;

    // Constructor
    public CommandHandler(Player player) {
        this.player = player;
    }

    //* Command helpers

    // Checks if it begins with a vowel
    public boolean beginsWithVowel(String text) {
        String firstLetter = text.substring(0, 1).toLowerCase();

        switch (firstLetter) {
            case "a":
            case "e":
            case "i":
            case "o":
            case "u":
                return true;
            default:
                return false;
        }
    }

    // Checks to see if there are duplicate enemies in the room and returns how many
    public int getAmountEnemyDuplicates(Enemy enemy, ArrayList<Enemy> enemies) {
        int num = 0;
        for (Enemy e : enemies) {
            if (e.equals(enemy)) {
                num++;
            }
        }
        return num;
    }

    // Checks to see whether or not you should put "a" or "an" in front of an object
    public String getAddressing(String word) {
        String text = "";
        if (beginsWithVowel(word)) {
            text = "an";
        } else {
            text = "a";
        }
        return text;
    }

    // Checks the objects position in the array list and returns a String with all objects concated
    public String getFormatedString(String name, String begText, ArrayList arrayList, boolean firstObject, int i) {
        String formated = "";
        String addressing = getAddressing(name);

        if (firstObject) {
            formated = String.format("%s %s %s", begText, addressing, name);
        } else {
            if (i < arrayList.size() - 1) {
                formated = String.format(", %s %s", addressing, name);
            } else {
                formated = String.format(", and %s %s", addressing, name);
            }
        }

        return formated;
    }

    // Checks the objects position in the array list and returns a String with all objects concated
    public String getFormatedString(String name, String begText, ArrayList arrayList, ArrayList duplicates, int numOfDup, boolean firstObject, int i) {
        String formated = "";
        String addressing = getAddressing(name);

        if (firstObject) {
            if (name.endsWith("s")) {
                formated = String.format("%s %s %ses", begText, numOfDup, name);
            } else {
                formated = String.format("%s %s %ss", begText, numOfDup, name);
            }
        } else {
            if (i < arrayList.size() - 1) {
                if (name.endsWith("s")) {
                    formated = String.format(", %s %ses", numOfDup, name);
                } else {
                    formated = String.format(", %s %ss", numOfDup, name);
                }
            } else {
                if (name.endsWith("s")) {
                    formated = String.format(", and %s %ses", numOfDup, name);
                } else {
                    formated = String.format(", and %s %ss", numOfDup, name);
                }
            }
        }

        return formated;
    }

    //* Command Section

    public Room exitTo(String dir) {
        for (Enumeration e = player.getCurrentRoom().getExits().elements(); e.hasMoreElements();) {
            Exit exit = (Exit) e.nextElement();

            if ((exit.getDirectionName().equals(dir) || (exit.getShortDirectionName().equals(dir)))) {
                return exit.getLeadsTo();
            }
        }
        return player.getCurrentRoom();
    }

    public String exitText(String dir) {
        for (Enumeration e = player.getCurrentRoom().getExits().elements(); e.hasMoreElements();) {
            Exit exit = (Exit) e.nextElement();

            if ((exit.getDirectionName().equals(dir) || (exit.getShortDirectionName().equals(dir)))) {
                return exit.getTravelText();
            }
        }
        return "No exit " + dir;
    }

    public String getInfo(String info) {
        boolean first;
        int i;
        switch (info) {
            case "n":
            case "name":
                return player.getName();
            case "fn":
            case "full name":
                return player.getFullName();
            case "loc":
            case "location":
                return player.getCurrentRoom().getTitle();
            case "desc":
            case "description":
                return player.getCurrentRoom().getDescription();
            case "he":
            case "health":
                return String.valueOf(player.getHealth());
            case "a":
            case "age":
                return String.valueOf(player.getAge());
            case "e":
            case "exits":
                return String.valueOf(player.getCurrentRoom().getExits());
            case "en":
            case "enemies":
                ArrayList<Enemy> currentEnemies = player.getCurrentRoom().getEnemies();
                ArrayList<Enemy> duplicateEnemies = new ArrayList<>();
                String text = "";
                i = 0;
                first = true;
                if (!currentEnemies.isEmpty()) {
                    // Loops through all the enemies in the room
                    for (Enemy enemy : currentEnemies) {
                        String name = enemy.getName();
                        boolean multipleEnemies = false;
                        int numOfDuplicateEnemies = 1;

                        // Checks to see if duplicate and finds how many
                        if ((currentEnemies.indexOf(enemy) != -1) && (currentEnemies.indexOf(enemy)!= currentEnemies.lastIndexOf(enemy))) {
                            multipleEnemies = true;
                            numOfDuplicateEnemies = getAmountEnemyDuplicates(enemy, currentEnemies);
                        }

                        // Checks for duplicate and if it hasn't been parsed yet
                        if (multipleEnemies) {
                            if (!duplicateEnemies.contains(enemy)) {
                                text += getFormatedString(name, "There are", currentEnemies, duplicateEnemies, numOfDuplicateEnemies, first, i);
                                duplicateEnemies.add(enemy);
                            }
                        } else {
                            text += getFormatedString(name, "There is", currentEnemies, first, i);
                        }
                        // Does the thing it need with variables (fix this comment later)
                        first = false;
                        i++;
                    }
                } else {
                    text = "There are no enemies in the room";
                }
                return text;
            case "h":
            case "hand":
            case "ci":
            case "current item":
                return String.format("You are holding %s %s", getAddressing(player.getCurrentItem().getName()), player.getCurrentItem().getName());
            case "inv":
            case "inventory":
                String items = "";
                first = true;
                i = 0;
                // Checks for empty array, gives message if empty
                if (!player.getInventory().isEmpty()) {
                    // Loops through all the items in the player's inventory
                    for (Item item : player.getInventory()) {
                        items += getFormatedString(item.getName(), "You have", player.getInventory(), first, i);
                        first = false;
                        i++;
                    }
                } else {
                    items = "You have no items in your inventory";
                }
                return items;
        }
        return "No info on " + info;
    }

}

