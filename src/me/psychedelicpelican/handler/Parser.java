package me.psychedelicpelican.handler;

import me.psychedelicpelican.Util.StringUtil;
import me.psychedelicpelican.story.Room;
import me.psychedelicpelican.player.Player;

/**
 * Created by Caidan Williams on 5/6/2016.
 */
public class Parser {

    // Object init
    Player player;
    CommandHandler commandHandler;

    // Constructor
    public Parser(Player player) {
        this.player = player;
        commandHandler = new CommandHandler(player);
    }

    /* Sort input commands */
    // Movement
    public String parseText(String text) {
        // See if text is a certain tag, then run tag's method
        if (text.contains("go ")) {
            String dir = text.substring(3); // Find where to go
            Room goingTo = commandHandler.exitTo(dir);

            // Combine location title and description
            String message = commandHandler.exitText(dir);
            if (!goingTo.getVisited()) {
                message += StringUtil.NEWLINE + "You see " + goingTo.getDescription();
                goingTo.setVisited(true);
            }

            player.setCurrentRoom(goingTo);

            return message;
        }
        if (text.contains("get ")) {
            String info = text.substring(4); // Find what to get
            return(commandHandler.getInfo(info));
        }
        return "";
    }



}