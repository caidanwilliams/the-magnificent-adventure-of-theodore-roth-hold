package me.psychedelicpelican.story;

import me.psychedelicpelican.map.Map;
import me.psychedelicpelican.story.places.personal.Airship;
import me.psychedelicpelican.story.places.towns.Williamsburg;
import me.psychedelicpelican.Util.StringUtil;

/**
 * Created by Caidan Williams on 5/8/2016.
 */
public class Story {

    // Contains all location
    Map map;

    String title = "The Magnificent Adventure of Theodore Roth-Hold";
    String openingMessage = "Welcome, Theodore";

    String playerName = "Theodore";
    String playerFullName = "Theodore Roth-Hold";
    Room playerStartingRoom;
    Item playerStartingItem;

    public Story(Map map) {
        this.map = map;

        /*** All story starts here ***/

        // Opening text
        setOpeningMessage(
                "You wake up, lying in bed and notice a few bites on your torso."
                + StringUtil.NEWLINE
                + "The bright sun is gleaming through the window to your left."
        );

        // personal
        Airship airship = new Airship(map);
        Williamsburg williamsburg = new Williamsburg(map);

        //* Items
        Item pocket_watch = new Item("Pocket Watch");
        Item coins = new Item("Coins", true);
        coins.setAmount(34);

        // us.caidan.player set starting stuffs
        Room sl = airship.getRoom("Bed");
        if (sl != null) {
            setPlayerStartingRoom(sl);
        }

        setPlayerStartingItem(coins);

        /*** All story stops here ***/
    }

    // Assign story title
    public void setTitle(String title) {
        title = title;
    }

    // Return story title
    public String getTitle() {
        return title;
    }

    // Assign story title
    public void setOpeningMessage(String message) {
        openingMessage = message;
    }

    // Return story title
    public String getOpeningMessage() {
        return openingMessage;
    }

    // Assign player name
    public void setPlayerName(String name) {
        playerName = name;
    }

    // Return player name
    public String getPlayerName() {
        return playerName;
    }

    // Assign player full name
    public void setPlayerFullName(String fullName) {
        playerFullName = fullName;
    }

    // Return player full name
    public String getPlayerFullName() {
        return playerFullName;
    }

    // Assign starting room
    public void setPlayerStartingRoom(Room room) {
        room.setVisited(true);
        playerStartingRoom = room;
    }

    // Return starting location
    public Room getPlayerStartingRoom() {
        return playerStartingRoom;
    }

    // Assign starting item
    public void setPlayerStartingItem(Item item) {
        playerStartingItem = item;
    }

    // Return starting item
    public Item getPlayerStartingItem() {
        return playerStartingItem;
    }

}
