package me.psychedelicpelican.story;

/**
 * Created by Caidan Williams on 5/7/2016.
 */
public class Item {

    // Class Variables
    private String name;
    private boolean isStackable;
    private int amount;

    // Partial constructor
    public Item(String name) {
        // Assign values
        this.name = name;
        isStackable = false;
        amount = 1;
    }

    // Full constructor
    public Item(String name, boolean stackable) {
        // Assign values
        this.name = name;
        isStackable = stackable;
        amount = 1;
    }


    // Assign name
    public void setName(String name) {
        this.name = name;
    }

    // Return name
    public String getName() {
        return name;
    }

    // Assign isStackable
    public void setStackable(boolean stackable) {
        isStackable = stackable;
    }

    // Return isStackable
    public boolean getStackable() {
        return isStackable;
    }

    // Add ammount
    public void addAmount(int num) {
        amount += num;
    }

    // Remove ammount
    public void removeAmount(int num) {
        if (amount - num > 0) {
            amount -= num;
        }
        else {
            amount = 0;
        }
    }

    // Assign set amount
    public void setAmount(int num) {
        amount = num;
    }

    // Return amount
    public int getAmount() {
        return amount;
    }

}
