package me.psychedelicpelican.story;

/**
 * Created by Caidan Williams on 5/7/2016.
 */
public class Enemy {

    // Class Variables
    private String name;
    private int health;
    private Room currentRoom;

    // Enemy constructor
    public Enemy(String name, int health) {
        // Assign input values
        this.name = name;
        currentRoom = null;
        this.health = health;
    }

    // Assign name
    public void setName(String name) {
        name = name;
    }

    // Return name
    public String getName() {
        return name;
    }

    // Add health
    public void addHealth(int amount) {
        health += amount;
    }

    // Remove health
    public void removeHealth(int amount) {
        health -= amount;
    }

    // Assign health
    public void setHealth(int amount) {
        health = amount;
    }

    // Return Health
    public int getHealth() {
        return health;
    }

    // Assign current room
    public void setCurrentRoom(Room room) {
        currentRoom = room;
    }

    // Return current location
    public Room getCurrentRoom() {
        return currentRoom;
    }

}
