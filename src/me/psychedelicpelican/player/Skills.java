package me.psychedelicpelican.player;

/**
 * Created by Caidan Williams on 5/7/2016.
 */
public class Skills {

    // us.caidan.player skills array
    private int[] skills = {
            1, // 0 Acrobatics
            1, // 1 Art
            1, // 2 Brawling
            1, // 3 Computers/Electronics
            1, // 4 Crime (Lock picking, Breaking and entering, etc.)
            1, // 5 Driving
            1, // 6 Ranged Weaponry
            1, // 7 Melee Weaponry
            1, // 8 Knowledge
            1, // 9 Languages
            1, // 10 Mechanics
            1, // 11 Medicine
            1, // 12 Influence
            1, // 13 Notice
            1, // 14 Occultism
            1, // 15 Science
            1, // 16 Sports
            1, // 17 Luck
            1, // 18 Adventuring
    };

    // Return skill position
    public int getSkillPosition(String name) {
        switch (name){
            case "Acrobatics":
                return 0;
            case "Art":
                return 1;
            case "Brawling":
                return 2;
            case "Computers":
                return 3;
            case "Crime":
                return 4;
            case "Driving":
                return 5;
            case "Ranged":
                return 6;
            case "Melee":
                return 7;
            case "Knowledge":
                return 8;
            case "Language":
                return 9;
            case "Mechanics":
                return 10;
            case "Medicine":
                return 11;
            case "Influence":
                return 12;
            case "Notice":
                return 13;
            case "Occultism":
                return 14;
            case "Science":
                return 15;
            case "Sports":
                return 16;
            case "Luck":
                return 17;
            case "Adventuring":
                return 18;
            default:
                return -1; // For error usage
        }
    }

    // Add skill point(s)
    public void addSkillPoint(String skill, int amount) {
        int pos = getSkillPosition(skill);
        if (pos != -1) {
            skills[pos] += amount;
        }
    }

    // Remove skill point(s)
    public void removeSkillPoint(String skill, int amount) {
        int pos = getSkillPosition(skill);
        if (pos != -1) {
            skills[pos] -= amount;
        }
    }

    // Assign skill point(s)
    public void setSkillPoint(String skill, int amount) {
        int pos = getSkillPosition(skill);
        if (pos != -1) {
            skills[pos] = amount;
        }
    }

    // Return skill point(s)
    public int getSkillPoints(String skill) {

        return -1; // Error
    }

}
