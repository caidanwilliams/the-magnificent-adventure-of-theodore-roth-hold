package me.psychedelicpelican.player;

import me.psychedelicpelican.story.Item;
import me.psychedelicpelican.story.Room;
import me.psychedelicpelican.story.Story;

import java.util.ArrayList;

/**
 * Created by Caidan Williams on 5/7/2016.
 */
public class Player {

    // Class Variables
    private String name;
    private String fullName;
    private int health;
    private int age;
    private Room currentRoom;
    private ArrayList<Item> inventory;
    private Item currentItem;
    private Skills skills;

    Story story;

    // us.caidan.player Constructor
    public Player(Story story) {
        // Assign empty values
        name = story.getPlayerName();
        fullName = story.getPlayerFullName();
        health = 5;
        age = 19;
        currentRoom = story.getPlayerStartingRoom();
        currentItem = story.getPlayerStartingItem();

        inventory = new ArrayList<>();
        inventory.add(getCurrentItem());
    }

    // Assign name
    public void setName(String name) {
        name = name;
    }

    // Return name
    public String getName() {
        return name;
    }

    // Assign full name
    public void setFullName(String fullName) {
        fullName = fullName;
    }

    // Return full name
    public String getFullName() {
        return fullName;
    }

    // Add health
    public void addHealth(int amount) {
        health += amount;
    }

    // Remove health
    public void removeHealth(int amount) {
        health -= amount;
    }

    // Assign health
    public void setHealth(int amount) {
        health = amount;
    }

    // Return Health
    public int getHealth() {
        return health;
    }

    // Add age
    public void addAge(int amount) {
        age += amount;
    }

    // Remove age
    public void removeAge(int amount) {
        age -= amount;
    }

    // Assign age
    public void setAge(int amount) {
        age = amount;
    }

    // Return Age
    public int getAge() {
        return age;
    }

    // Assign current room
    public void setCurrentRoom(Room room) {
        currentRoom = room;
    }

    // Return current location
    public Room getCurrentRoom() {
        return currentRoom;
    }

    // Assign current item
    public void setCurrentItem(Item item) {
        currentItem = item;
    }

    // Return current item
    public Item getCurrentItem() {
        return currentItem;
    }

    // Add item to inventory
    public void addToInv(Item item, int amount) {
        if (item.getStackable()) {
            item.setAmount(item.getAmount() + amount); // Add amount to item
        }
        else {
            inventory.add(item); // Add a new item to inventory or alongside its self
        }
    }

    // Remove item from inventory
    public void removeFromInv(Item item, int amount) {
        if (inventory.contains(item)) {
            item.setAmount(item.getAmount() - amount);
            if (item.getAmount() <= 0) {
                inventory.remove(item);
            }
        }
    }

    // Assign inventory
    public void setInventory(ArrayList<Item> inv) {
        inventory = inv;
    }

    // Return inventory
    public ArrayList<Item> getInventory() {
        return inventory;
    }

}
