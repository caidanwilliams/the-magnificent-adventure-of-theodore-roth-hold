package me.psychedelicpelican.map;

import me.psychedelicpelican.story.Room;

import java.util.ArrayList;

/**
 * Created by Caidan Williams on 5/7/2016.
 */
public class Map {

    ArrayList<Room> map;

    public Map(ArrayList<Room> m) {
        map = m;
    }

    // Assign map
    public void setMap(ArrayList<Room> map) {
        this.map = map;
    }

    // Returns map
    public ArrayList<Room> getMap() {
        return map;
    }

    // Append to map
    public void addLocation(Room room) {
        map.add(room);
    }

    // Remove from map
    public void removeLocation(Room room) {
        map.remove(room);
    }

    // Find location
    public Room getLocation(String name) {
        for (Room room : map) {
            if (name.equals(room.getTitle())) {
                return room;
            }
        }
        return new Room("Error", "Please fix this, this location shouldn't exist...");
    }

}
