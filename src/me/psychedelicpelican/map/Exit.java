package me.psychedelicpelican.map;

import me.psychedelicpelican.story.Room;

/**
 * Created by Caidan Williams on 5/6/2016.
 *
 * Represents an exit to a location
 *
 */
public class Exit {

    // Numerical codes
    public static final int undefined = 0;
    public static final int north = 1;
    public static final int south = 2;
    public static final int east = 3;
    public static final int west = 4;
    public static final int up = 5;
    public static final int down  = 6;
    public static final int northeast = 7;
    public static final int northwest = 8;
    public static final int southeast = 9;
    public static final int southwest = 10;
    public static final int in = 11;
    public static final int out = 12;

    // String codes
    public static final String[] dirName = {
            "undefined",
            "north",
            "south",
            "east",
            "west",
            "up",
            "down",
            "northeast",
            "northwest",
            "southeast",
            "southwest",
            "in",
            "out"
    };

    public static final String[] shortDirName = {
            "null",
            "n",
            "s",
            "e",
            "w",
            "u",
            "d",
            "ne",
            "nw",
            "se",
            "sw",
            "i",
            "o",
    };

    // Class Variables
    private Room leadsTo = null;
    private int direction;
    private String travelText;

    // Full name of direction eg. northeast
    private String directionName;

    // Shortened name of direction eg. ne
    private String shortDirectionName;

    // Default constructor
    public Exit() {
        direction = Exit.undefined;
        leadsTo = null;
        directionName = dirName[undefined];
        shortDirectionName = shortDirName[undefined];
    }

    // Full constructor
    public Exit(int dir, Room lT, String text) {
        // Assign direction
        direction = dir;

        // Assign direction names
        if (direction <= dirName.length) directionName = dirName[direction];
        if (direction <= shortDirName.length) shortDirectionName = shortDirName[direction];

        // Assign location
        leadsTo = lT;

        // Assign travel text
        travelText = text;
    }

    // toString method
    public String toString() {
        return directionName;
    }

    // Assigns direction name
    public void setDirectionName(String name) {
        directionName = name;
    }

    // Returns direction name
    public String getDirectionName() {
        return directionName;
    }

    // Assigns short direction name
    public void setShortDirName(String shortName) {
        shortDirectionName = shortName;
    }

    // Returns short direction name
    public String getShortDirectionName() {
        return shortDirectionName;
    }

    // Assign location
    public void setLeadsTo(Room lT) {
        leadsTo = lT;
    }

    // Returns location
    public Room getLeadsTo() {
        return leadsTo;
    }

    // Assign travel text
    public void setTravelText(String text) {
        travelText = text;
    }

    // Return travel text
    public String getTravelText() {
        return travelText;
    }

}
