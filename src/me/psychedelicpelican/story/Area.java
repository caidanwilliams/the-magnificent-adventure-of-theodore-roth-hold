package me.psychedelicpelican.story;

import java.util.LinkedList;

/**
 * Created by Caidan Williams on 5/15/2016.
 */
public class Area {

    protected LinkedList<Room> rooms;

    public Area(LinkedList list) {
        rooms = list;
    }

    public Room getRoom(String title) {
        for (Room room: rooms) {
            if (room.getTitle().equalsIgnoreCase(title)) {
                return room;
            }
        }
        return null;
    }

}
