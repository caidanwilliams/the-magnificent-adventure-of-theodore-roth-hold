package me.psychedelicpelican.story.places.wilderness;

import me.psychedelicpelican.map.Exit;
import me.psychedelicpelican.map.Map;
import me.psychedelicpelican.story.Area;
import me.psychedelicpelican.story.Enemy;
import me.psychedelicpelican.story.Room;

import java.util.LinkedList;

/**
 * Created by Caidan Williams on 6/3/2016.
 */
public class Forest extends Area {

    private Map map;

    // Rooms in the area
    private Room center, north, northeast, east, southeast, south, southwest, west, northwest;

    public Forest(Map map) {
        super(new LinkedList());

        this.map = map;

        //* Rooms
        center = new Room("Center Forest", "it's the middle of the visible forest.");
        north = new Room("North Forest", "it's the north part of the visible forest.");
        northeast = new Room("North East Forest", "it's the north east part of the visible forest.");
        east = new Room("East Forest", "it's the east part of the visible forest.");
        southeast = new Room("South East Forest", "it's the south east part of the visible forest.");
        south = new Room("South Forest", "it's the south part of the visible forest.");
        southwest = new Room("South West Forest", "it's the south west part of the visible forest.");
        west = new Room("West Forest", "it's the west part of the visible forest.");
        northwest = new Room("North West Forest", "it's the north west part of the visible forest.");

        // Add rooms to rooms list
        rooms.add(center);
        rooms.add(north);
        rooms.add(northeast);
        rooms.add(east);
        rooms.add(southeast);
        rooms.add(south);
        rooms.add(southwest);
        rooms.add(west);
        rooms.add(northwest);

        // Add rooms to map
        rooms.forEach(map::addLocation);

        /* Exits */
        // Center
        Exit c_n = new Exit(Exit.north, north, "Traveling in forest");
        Exit c_ne = new Exit(Exit.northeast, northeast, "Traveling in forest");
        Exit c_e = new Exit(Exit.east, east, "Traveling in forest");
        Exit c_se = new Exit(Exit.southeast, southeast, "Traveling in forest");
        Exit c_s = new Exit(Exit.south, south, "Traveling in forest");
        Exit c_sw = new Exit(Exit.southwest, southwest, "Traveling in forest");
        Exit c_w = new Exit(Exit.west, west, "Traveling in forest");
        Exit c_nw = new Exit(Exit.northwest, northwest, "Traveling in forest");

        // North
        Exit n_e = new Exit(Exit.east, northeast, "Traveling in forest");
        Exit n_se = new Exit(Exit.southeast, east, "Traveling in forest");
        Exit n_s = new Exit(Exit.south, center, "Traveling in forest");
        Exit n_sw = new Exit(Exit.southwest, west, "Traveling in forest");
        Exit n_w = new Exit(Exit.west, northwest, "Traveling in forest");

        // North East
        Exit ne_s = new Exit(Exit.south, east, "Traveling in forest");
        Exit ne_sw = new Exit(Exit.southwest, center, "Traveling in forest");
        Exit ne_w = new Exit(Exit.west, north, "Traveling in forest");
        
        // East
        Exit e_n = new Exit(Exit.north, northeast, "Traveling in forest");
        Exit e_s = new Exit(Exit.south, southeast, "Traveling in forest");
        Exit e_sw = new Exit(Exit.southwest, south, "Traveling in forest");
        Exit e_w = new Exit(Exit.west, center, "Traveling in forest");
        Exit e_nw = new Exit(Exit.northwest, north, "Traveling in forest");

        // South East
        Exit se_n = new Exit(Exit.north, east, "Traveling in forest");
        Exit se_w = new Exit(Exit.west, south, "Traveling in forest");
        Exit se_nw = new Exit(Exit.northwest, center, "Traveling in forest");

        // South
        Exit s_n = new Exit(Exit.north, center, "Traveling in forest");
        Exit s_ne = new Exit(Exit.northeast, east, "Traveling in forest");
        Exit s_e = new Exit(Exit.east, southeast, "Traveling in forest");
        Exit s_w = new Exit(Exit.west, southwest, "Traveling in forest");
        Exit s_nw = new Exit(Exit.northwest, west, "Traveling in forest");

        // South West
        Exit sw_n = new Exit(Exit.north, west, "Traveling in forest");
        Exit sw_ne = new Exit(Exit.northeast, center, "Traveling in forest");
        Exit sw_e = new Exit(Exit.east, south, "Traveling in forest");

        // West
        Exit w_n = new Exit(Exit.north, northwest, "Traveling in forest");
        Exit w_ne = new Exit(Exit.northeast, north, "Traveling in forest");
        Exit w_e = new Exit(Exit.east, center, "Traveling in forest");
        Exit w_se = new Exit(Exit.southeast, south, "Traveling in forest");
        Exit w_s = new Exit(Exit.south, southwest, "Traveling in forest");

        // North West
        Exit nw_e = new Exit(Exit.east, north, "Traveling in forest");
        Exit nw_se = new Exit(Exit.southeast, center, "Traveling in forest");
        Exit nw_s = new Exit(Exit.south, west, "Traveling in forest");

        // Add exits to rooms
        center.addExit(c_n);
        center.addExit(c_ne);
        center.addExit(c_e);
        center.addExit(c_se);
        center.addExit(c_s);
        center.addExit(c_sw);
        center.addExit(c_w);
        center.addExit(c_nw);

        north.addExit(n_e);
        north.addExit(n_se);
        north.addExit(n_s);
        north.addExit(n_sw);
        north.addExit(n_w);

        northeast.addExit(ne_s);
        northeast.addExit(ne_sw);
        northeast.addExit(ne_w);

        east.addExit(e_n);
        east.addExit(e_s);
        east.addExit(e_sw);
        east.addExit(e_w);
        east.addExit(e_nw);

        southeast.addExit(se_n);
        southeast.addExit(se_w);
        southeast.addExit(se_nw);

        south.addExit(s_n);
        south.addExit(s_ne);
        south.addExit(s_e);
        south.addExit(s_w);
        south.addExit(s_nw);

        southwest.addExit(sw_n);
        southwest.addExit(sw_ne);
        southwest.addExit(sw_e);

        west.addExit(w_n);
        west.addExit(w_ne);
        west.addExit(w_e);
        west.addExit(w_se);
        west.addExit(w_s);

        northwest.addExit(nw_e);
        northwest.addExit(nw_se);
        northwest.addExit(nw_s);

        //* Enemies
        Enemy monkey = new Enemy("Monkey", 3);

        // Add enemies to locations
        center.addEnemy(monkey);
        center.addEnemy(monkey);
    }

}
