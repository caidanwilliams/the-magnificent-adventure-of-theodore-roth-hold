package me.psychedelicpelican.story.places.personal;

import me.psychedelicpelican.map.Exit;
import me.psychedelicpelican.map.Map;
import me.psychedelicpelican.story.Room;
import me.psychedelicpelican.story.Area;

import java.util.LinkedList;

/**
 * Created by Caidan Williams on 5/15/2016.
 */
public class Airship extends Area {

    private Map map;

    // Rooms in the area
    private Room bed, bedroom, window, hallway, storage, captains, hatch;

    private Room currentCity;

    public Airship(Map map) {
        super(new LinkedList());
        this.map = map;

        //* personal
        bed = new Room("Bed", "your old bed, worn out an dusty, but it's your bed.");
        bedroom = new Room("Bed Room", "your room, containing all of your stuff, like a normal person.");
        window = new Room("Window", "the ground, coming fast at you. You died.");
        hallway = new Room("Hallway", "a hallway that connects to other rooms.");
        storage = new Room("Storage Room", "it's used for holding all equipment and valuables.");
        captains = new Room("Captain's Chair", "a captain's chair, used to navigate the airship.");
        hatch = new Room("Hatch", "a town with many people.");

        // Add locations to rooms
        rooms.add(bed);
        rooms.add(bedroom);
        rooms.add(window);
        rooms.add(hallway);
        rooms.add(storage);
        rooms.add(captains);
        rooms.add(hatch);

        // Add rooms to map
        rooms.forEach(map::addLocation);

        //* Exits
        Exit bed_up = new Exit(Exit.up, bedroom, "Getting out of bed");

        Exit bedroom_down = new Exit(Exit.down, bed, "Getting in bed");
        Exit bedroom_north = new Exit(Exit.north, window, "Jumping out window");
        Exit bedroom_south = new Exit(Exit.south, hallway, "Going into the hallway");

        Exit hallway_west = new Exit(Exit.west, bedroom, "Going into your bedroom");
        Exit hallway_east = new Exit(Exit.east, storage, "Squeezing into the storage room");
        Exit hallway_north = new Exit(Exit.north, captains, "Walking to the captain's chair");
        Exit hallway_south = new Exit(Exit.south, hatch, "Opening hatch");

        Exit storage_south = new Exit(Exit.south, hallway, "Going into the hallway");

        Exit captains_south = new Exit(Exit.south, hallway, "Going into the hallway");

        Exit hatch_down = new Exit(Exit.down, currentCity, "Going to land");

        // Add exits to locations
        bed.addExit(bed_up);

        bedroom.addExit(bedroom_down);
        bedroom.addExit(bedroom_north);
        bedroom.addExit(bedroom_south);

        hallway.addExit(hallway_west);
        hallway.addExit(hallway_east);
        hallway.addExit(hallway_north);
        hallway.addExit(hallway_south);

        storage.addExit(storage_south);

        captains.addExit(captains_south);

        hatch.addExit(hatch_down);

        //* Enemies
            // None right now

        // Add enemies to locations
            // Sorry no enemies to add
    }

    // Set current city
    public void setCurrentCity(Room loc) {
        currentCity = loc;
    }

    // Get current city
    public Room getCurrentCity() {
        return currentCity;
    }

}
