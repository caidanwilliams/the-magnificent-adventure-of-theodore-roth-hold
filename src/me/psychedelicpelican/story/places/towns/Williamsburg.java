package me.psychedelicpelican.story.places.towns;

import me.psychedelicpelican.map.Exit;
import me.psychedelicpelican.map.Map;
import me.psychedelicpelican.story.Area;
import me.psychedelicpelican.story.Room;

import java.util.LinkedList;

/**
 * Created by Caidan Williams on 5/16/2016.
 */
public class Williamsburg extends Area {

    private Map map;

    // Rooms in the area
    private Room townsquare, gunsmith, blacksmith, armory, hattery;

    public Williamsburg(Map map) {
        super(new LinkedList());

        this.map = map;

        //* Rooms
        townsquare = new Room("Williamsburg towns Square", "it's actually more circular than square.");
        gunsmith = new Room("Clockwork Gun Smith", "you can buy and sell guns here.");
        blacksmith = new Room("Red Rod Black Smith", "you can buy and sell weapons here.");
        armory = new Room("Williamsburg Armory", "you can buy and sell armor here.");
        hattery = new Room("Tom's Hattery", "you can buy and sell hats here.");

        // Add rooms to rooms list
        rooms.add(townsquare);
        rooms.add(gunsmith);
        rooms.add(blacksmith);
        rooms.add(armory);
        rooms.add(hattery);

        // Add rooms to map
        rooms.forEach(map::addLocation);

        //* Exits
        Exit ts_e = new Exit(Exit.east, gunsmith, "Ducking into the Clockwork Gun Smith");
        Exit ts_ne = new Exit(Exit.northeast, blacksmith, "Going into the Red Rod Black Smith");
        Exit ts_w = new Exit(Exit.west, armory, "Walking to the Williamsburg Armory");
        Exit ts_nw = new Exit(Exit.northwest, hattery, "Galloping to Tom's Hattery");
        Exit ts_u = new Exit(Exit.up, map.getLocation("Hatch"), "Climbing into Airship");

        Exit exit_s_to_ts = new Exit(Exit.south, townsquare, "Going to Williamsburg towns Square");

        // Add exits to rooms
        townsquare.addExit(ts_e);
        townsquare.addExit(ts_ne);
        townsquare.addExit(ts_w);
        townsquare.addExit(ts_nw);
        townsquare.addExit(ts_u);

        gunsmith.addExit(exit_s_to_ts);

        blacksmith.addExit(exit_s_to_ts);

        armory.addExit(exit_s_to_ts);

        hattery.addExit(exit_s_to_ts);

        //* Enemies
            // None right now

        // Add enemies to locations
            // Sorry no enemies to add
    }

}
