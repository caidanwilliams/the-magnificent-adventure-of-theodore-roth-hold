package me.psychedelicpelican.story;

import me.psychedelicpelican.map.Exit;

import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by Caidan Williams on 5/6/2016.
 *
 * Represents a location for the game
 *
 */
public class Room {

    // Class Variables
    private String locationTitle;
    private String locationDescription;
    private boolean visited;
    private Vector vecExits;
    private ArrayList<Enemy> enemies;

    // Room Constructor
    public Room(String title, String description) {
        // Assign title and description
        locationTitle = title;
        locationDescription = description;
        visited = false;

        // Blank Vector & ArrayList
        vecExits = new Vector();
        enemies = new ArrayList<>();
    }

    // toString method
    public String toString() {
        return locationTitle;
    }

    // Adds an exit to the room/location
    public void addExit(Exit exit) {
        vecExits.addElement(exit);
    }

    // Removes an exit from the room/location
    public void removeExit(Exit exit) {
        if (vecExits.contains(exit)) {
            vecExits.removeElement(exit);
        }
    }

    // Return vector of exits
    public Vector getExits() {
        // Return a clone so we don't give direct access
        return (Vector) vecExits.clone();
    }

    // Adds an exit to the room/location
    public void addEnemy(Enemy enemy) {
        enemy.setCurrentRoom(this);
        if (!enemies.contains(enemy)) {
            enemies.add(enemy);
        } else {
            // Allows adding multiple enemies
            Enemy dup = enemy;
            enemies.add(dup);
        }
    }

    // Removes an exit from the room/location
    public void removeEnemy(Enemy enemy) {
        if (enemies.contains(enemy)) {
            enemies.remove(enemy);
        }
    }

    // Return vector of exits
    public ArrayList<Enemy> getEnemies() {
        // Return a clone so we don't give direct access
        return (ArrayList<Enemy>) enemies.clone();
    }

    // Return location title
    public String getTitle() {
        return locationTitle;
    }

    // Assigns location title
    public void setTitle(String title) {
        locationTitle = title;
    }

    // Returns location description
    public String getDescription() {
        return locationDescription;
    }

    // Assign location description
    public void setDescription(String description) {
        locationDescription = description;
    }

    // Assign visited
    public void setVisited(boolean bool) {
        visited = bool;
    }

    // Return visited
    public boolean getVisited() {
        return visited;
    }

}
