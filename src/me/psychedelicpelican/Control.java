package me.psychedelicpelican;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import me.psychedelicpelican.handler.Parser;
import me.psychedelicpelican.Util.StringUtil;

/**
 * Created by Caidan Williams on 5/7/2016.
 */
public class Control implements EventHandler<ActionEvent> {

    GUI gui;
    Parser parser;

    public Control(GUI gui, Parser parser) {
        this.gui = gui;
        this.parser = parser;
    }

    @Override
    public void handle(ActionEvent event) {
        // Find where the event is coming from
        Object cmd = event.getSource();

        // True if the event is coming from the button
        if (gui.button.equals(cmd)) {
            addText(parser.parseText(userInput())); // Parse text
            //System.out.println("You pressed the button, YAY!"); // Debugging purpose at this point
        }
    }

    // Get user input
    public String userInput() {
        return gui.commandField.getText(); // Get text from user
    }

    // Append parsed text
    public void addText(String message) {
        // Evaluate parsed text
        if (!message.equals("")) {
            gui.commandField.setText(""); // Set the text back to nothing, giving the illusion it has been sent
            gui.console.appendText(message); // Add the message to the console so user can read
        } else {
            gui.console.appendText("command does not exist"); // Inform user the command does not exist
        }
        gui.console.appendText(StringUtil.NEWLINE);
    }

}