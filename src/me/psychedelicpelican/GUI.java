package me.psychedelicpelican;

import me.psychedelicpelican.map.Map;
import me.psychedelicpelican.player.Player;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import me.psychedelicpelican.handler.Parser;
import me.psychedelicpelican.story.Room;
import me.psychedelicpelican.story.Story;
import me.psychedelicpelican.Util.StringUtil;

import java.util.ArrayList;

/**
 * Created by Caidan Williams on 5/6/2016.
 */
public class GUI extends Application {

    // handler and Output objects
    TextArea console;
    TextField commandField;
    Button button;
    Scene scene;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Classes
        Map map = new Map(new ArrayList<Room>()); // map for player to travel around in
        Story story = new Story(map); // story to modify the locations, exits, and items
        Player player = new Player(story); // To help player keep organization
        Parser parser = new Parser(player); // Parsing user input to make sense of it
        Control control = new Control(this, parser);

        // Set title
        primaryStage.setTitle(story.getTitle());

        // New Grid layout
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER_LEFT);
        grid.setHgap(10);
        grid.setVgap(15);
        grid.setPadding(new Insets(25, 25, 25, 25));

        // Output console
        console = new TextArea(story.getOpeningMessage() + StringUtil.NEWLINE);
        console.setMinSize(400, 350);
        console.setWrapText(true);
        console.setEditable(false);
        console.setScrollTop(Double.MAX_VALUE);
        grid.add(console, 0, 0);

        // Command input
        commandField = new TextField();
        commandField.setEditable(true);
        grid.add(commandField, 0, 1);

        // Button (also keycode)
        button = new Button("Enter");
        grid.add(button, 1, 1);

        // Handle button press
        button.setOnAction(control);

        scene = new Scene(grid);
        //scene.getStylesheets().add("assets/theme.css");

        // Handle key press 'enter'
        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    control.addText(parser.parseText(control.userInput())); // Parse text
                    //System.out.println("You pressed the enter key, YAY!"); // Debugging purpose at this point
                }
            }
        });

        // Set scene
        primaryStage.setScene(scene);

        // Start (show) stage
        primaryStage.show();
    }

}
